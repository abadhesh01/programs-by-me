
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Check all the elements of the given array are conequtive or not.

// For more about consequtive numbers, refer to:
// https://www.splashlearn.com/math-vocabulary/number-sense/consecutive-numbers

public class MyCode17
{
  // The below method checks all the elements of the given array are conequtive or not.
  public static boolean checkConsequtive(int[] array)
  {
    // If the length of the given array is less thyan 3, then it's by default consequtive.
    // So, returning 'true'.
    if(array.length < 3)
        return true;
  
    // Calulating the difference between the element at index 0 and index 1 
    // of the given array.     
    int difference = array[array.length - 1] - array[array.length - 2];   
    
    // If the difference between the consequtive elements of the given array
    // are not the same, then only returning 'false'.
    for(int index = array.length - 3; index > 0; index --) 
        if((array[index] - array[index - 1]) != difference)
           return false;

    // Returning 'true' as the above testcases has been passed.
    return true;
  }  


  public static void main(String[] args) 
  {
     // Testing the method checkConsequtive() and printing the output on the console.
     System.out.println("\nChecking for the given array is consequtive or not.........\n");

     int[][] testcases = {
       {},
       {9},
       {0, 9},
       {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
       {1, 3, 5, 7, 9},
       {0, 2, 4, 6, 8},
       {1, 5, 2, 6, 3, 7, 4, 8, 5, 0},
       {9, 6, 6, 8, 1, 9, 4, 4, 6, 4},
       {8, 2, 6, 0, 2, 3, 8, 3, 1, 1},
       {9, 9, 9, 9, 9, 9, 9, 9, 9, 9}
     };

     for(int row = 0; row < testcases.length; row ++)
     {
       int[] testcase = testcases[row];
       System.out.print("Array " + (row + 1) + ": "); 
       MyCode05.printArray(testcase);
       System.out.println("Consequtive: " + checkConsequtive(testcase));
       System.out.println("------------------------------" 
                        + "-----------------------------\n");
     }
  }
}
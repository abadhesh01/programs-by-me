
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Generate a random string of random size.

import java.util.concurrent.ThreadLocalRandom;

public class MyCode03 {

    // The below function generates a random string 
    // of random size between 5 to 12.
    public static String getRandomString()
    {
        // Generating a random string.....
        String text = "";
        // Generating the length of the string.....
        int charcaterCount = ThreadLocalRandom.current().nextInt(5, 13);
        for(int count = 0; count < charcaterCount; count ++)
        {
            // Generating each characater and concartinating to the random string.....
            char textCharacter = (char) ThreadLocalRandom.current().nextInt(65, 91);
            text += textCharacter;
        }

        // returning the string.....
        return text;
    }

    public static void main(String[] args) {
        System.out.println("\nRandom String: " + getRandomString() + "\n");
    }
}

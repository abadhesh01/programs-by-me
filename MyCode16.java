
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Return the fibonacci sequence within a given range.

public class MyCode16 {

  // The below method returns the fibonacci sequence within a given range.
  public static java.util.List<Integer> fibonacciSequenceGenerator(int range)
  {

      java.util.List<Integer> fibbonacciSequence = new java.util.LinkedList<>();

      int firstFibonacci = 0;
      int lastFibonacci = 1;

      while(lastFibonacci <= range)
      {
        fibbonacciSequence.add(firstFibonacci);
        fibbonacciSequence.add(lastFibonacci);
        //System.out.print(firstFibonacci + " >>> " + lastFibonacci + " >>> ");
        firstFibonacci = firstFibonacci + lastFibonacci;
        lastFibonacci = firstFibonacci + lastFibonacci;
      }

      return fibbonacciSequence;
  }  

  public static void main(String[] args) 
  {
     System.out.println("\nFibbonacci sequence generator:");

     // Creating a scanner object to take the keyboard input. 
     java.util.Scanner scanner = new java.util.Scanner(System.in);

     // Input:
     System.out.print("\nEnter the range of the sequence: ");
     int range = scanner.nextInt();
     
     // Closing the scanner object to prevent resource leak.
     scanner.close();

     // Generating the result.
     java.util.List<Integer> fibbonacciSequence = fibonacciSequenceGenerator(range);
     
     // Printing the output.
     System.out.println("\nFibonacci sequence within '" + range 
     + "': " + fibbonacciSequence + "\n");
  }    
}


// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Check the given string is palindrome or not.

public class MyCode11
{

 // The below method checks the given string is palindrome or not.   
 public static boolean checkPalindrome(String string)
 {
    // Declaring and initializing the starting index of the given string.
    int startIndex = 0; 

    // Declaring and initializing the ending index of the given string.
    int endIndex = string.length() - 1;

    // Iterating untill start and end index either meet or cross each other.
    while(startIndex < endIndex) 
    {
       // Return 'false' if any of the starting and ending index value pair        
       // does not match each other. 
       if(string.charAt(startIndex) != string.charAt(endIndex))
          return false;   

        // Increment the start index by 1 towards the right hand side of the given array.  
        startIndex ++; 

        // Decrement the end index by 1 towards the left hand side of the given array.
        endIndex --;
    }

    // Return 'true' as all test cases has been passed.
    return true;
 }  

  public static void main(String[] args) {

    // Test cases to check wheather the method checkPalindrome() 
    // is working perfectly or not.
    String testCases[] = new String[10]; 
    testCases[0] = "MALAYALAM";
    testCases[1] = "malayalam";
    testCases[2] = "Malayalam";
    testCases[3] = "ROTOR";
    testCases[4] = "racecar";
    testCases[5] = "madam";
    testCases[6] = "dad";
    testCases[7] = "ram";
    testCases[8] = "Kamaz";
    testCases[9] = "STAR";

    // Testing and printing the output.
    System.out.println();
    System.out.printf("%-10s \t\t %s \n", "STRING", "IS PALINDROME");
    System.out.printf("%-10s \t\t %s \n", "------", "--------------");
    for (String string : testCases) 
        System.out.printf("%-10s \t\t %s \n", string, checkPalindrome(string));
    System.out.println();

  }
}
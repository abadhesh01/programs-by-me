
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Reverse the words in a given string.

public class MyCode07 {

    // The below function trims both the ends of a string.
    public static String trimText(String text)
    {
        int start = 0;
        int end = text.length() - 1;

        while(text.charAt(start) == ' ')
              start ++;

        while(text.charAt(end) == ' ')
              end --;      

        return text.substring(start, end + 1);
    }

    // The below function reverses the words of a given string.
    public static String reverseWords(String text)
    {
        String result = ""; // This string is used to concatenate words. 

        text = trimText(text); // Trimming the string from both the end.

        /* Assigning the last index of the string as first index 
         * of a word to the variable named 'first' and [(last index of 
         * the string) + 1] as last index of a word to the variable named 'last'.*/   
        int first = text.length() -1, last = text.length();

        while(first >= 0 /* Iterating through each
         indices of the string in a reverse order.*/)
        {
            /* Finding whether the current index     
             * is first character of a word. */
            if((first != 0) && 
               (text.charAt(first) != ' ') &&
               (text.charAt(first - 1) == ' '))
            {
                // Concatenating the substring using first and last index
                result += text.substring(first, last) + " ";
                // Updating the last index.
                last = first - 1;
                // Removing additional blank spaces if there is any. 
                while(text.charAt(last) == ' ')
                     last --;
                last ++;     
            }   

            /* The below statement is used for concatenating 
             * the first word of the string to the result only.*/ 
            if((first == 0))
                result += text.substring(first, last);
            
            // Updating the first index.
            first --;
        }

        // Return the result.
        return result;
    }

    public static void main(String[] args) {

        // Creating a scanner object to take keyboard input.....
        java.util.Scanner scanner = new java.util.Scanner(System.in);

        // Input.....
        System.out.println("\nEnter your text: ");
        String text = scanner.nextLine();

        // Printing the input.....
        System.out.println("\nYour text: ");
        System.out.println("\"" + text + "\"");
        
        // Reverseing the words.....
        System.out.println("\nReverseing the words.....");
        text = reverseWords(text);

        // Printing the output.....
        System.out.println("\nYour text: ");
        System.out.println("\"" + text + "\"");

        System.out.println();
        
        // Closing the scanner object.....
        scanner.close();
    }
}

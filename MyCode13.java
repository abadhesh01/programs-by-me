
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Odd Even arrangement. (Keep all the odd numbers on one side 
//          and all the even numbers to anathor side of the given array.)

 public class MyCode13
 {
    // The below method keeps all the odd numbers on one side and 
    // all the even numbers to anathor side of the given array.
    public static int[] separateOddEven(int[] array)
    { 
        // If the array has no length, then
        // return the array.
        if(array.length == 0)
           return array;
      
        // The below variables will keep track of total number 
        // of odd numbers and even numbers in the given array. 
        int oddCount = 0, evenCount = 0; 

        // Counting the total number of odd numbers  
        // and total number of even numbers.
        for(int number : array) 
           if(number % 2 == 0)
              evenCount ++;
           else 
              oddCount ++;

        // If the array contains neither odd numbers  
        // nor even numbers, then returning the array.      
        if(evenCount == 0 || oddCount == 0)
           return array;
            
        // Creating a new array of size about total number 
        // of odd numbers to filter and store all the odd
        // numbers from the given array to this array.    
        int[] arrayWithOddNumbers = new int[oddCount];
        // Creating a new array of size about total number 
        // of even numbers to filter and store all the even
        // numbers from the given array to this array.    
        int[] arrayWithEvenNumbers = new int[evenCount];             

        // Copying all the odd numbers from the given array to 
        // the array for odd numbers and even numbers to the array
        // for even numbers. 
        int oddNumberArrayIndex = 0, evenNumberArrayIndex = 0;
        for(int number : array)
           if(number % 2 == 0)
              arrayWithEvenNumbers[evenNumberArrayIndex ++] = number;
           else 
              arrayWithOddNumbers[oddNumberArrayIndex ++] = number;

        // The below statements are only for tracing purpose.
        // Comment them after they are used.      
        System.out.print("Array for odd numbers: ");
        MyCode05.printArray(arrayWithOddNumbers);
        System.out.print("Array for even numbers: ");
        MyCode05.printArray(arrayWithEvenNumbers);
      

        // Copying all the odd numbers from the odd number array to the left hand side 
        // of the given array.      
        System.arraycopy(arrayWithOddNumbers, 0, array, 0, oddCount);      
        // Copying all the even numbers from the even number array to the right hand side 
        // of the given array.
        System.arraycopy(arrayWithEvenNumbers, 0, array, oddCount, evenCount);

       // Returning the array 
       return array;
    }

    public static void main(String[] args) 
    {
        // Testing the method separateOddEven() and displaying the output.
        System.out.println("\nSeparating all the odd numbers and even numbers to " + 
        "left hand side and right hand side of the given array respectively.....");

        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.print("\nGiven array: ");
        MyCode05.printArray(array);
        array = separateOddEven(array);
        System.out.print("Given array after separating odd and even numbers: ");
        MyCode05.printArray(array);

        for(int count = 0; count < 4; count ++) 
        {
            System.out.println("---------------------------------------" + 
                               "---------------------------------------");
            array = MyCode05.getRandomIntegerArray(10);
            System.out.print("Given array: ");
            MyCode05.printArray(array);
            array = separateOddEven(array);
            System.out.print("Given array after separating odd and even numbers: ");
            MyCode05.printArray(array);        
        }

        System.out.print("---------------------------------------" + 
                           "---------------------------------------");
        array = new int[] {0, 2, 4, 6, 8};
        System.out.print("\nGiven array: ");
        MyCode05.printArray(array);
        array = separateOddEven(array);
        System.out.print("Given array after separating odd and even numbers: ");
        MyCode05.printArray(array);

        System.out.print("---------------------------------------" + 
                           "---------------------------------------");
        array = new int[] {1, 3, 5, 7, 9};
        System.out.print("\nGiven array: ");
        MyCode05.printArray(array);
        array = separateOddEven(array);
        System.out.print("Given array after separating odd and even numbers: ");
        MyCode05.printArray(array);

        System.out.print("---------------------------------------" + 
                           "---------------------------------------");
        array = new int[] {};
        System.out.print("\nGiven array: ");
        MyCode05.printArray(array);
        array = separateOddEven(array);
        System.out.print("Given array after separating odd and even numbers: ");
        MyCode05.printArray(array);
    }
 }

// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Generate a random integer array of given size.

import java.util.concurrent.ThreadLocalRandom;

public class MyCode05 {

   // The below function generates an array of random integers (between -100 to 100)
   // of given size.
   public static int[] getRandomIntegerArray(int size)
   {
      // Returning 'null' if the length of the array is less than or equal to 0.
      if(size <= 0)
        return null;

      // Creating an array of the given size.
      int array[] = new int[size];
      
      // Adding random numbers (between -100 to 100) to the array. 
      for (int index = 0; index < size; index ++)
           array[index] = ThreadLocalRandom.current().nextInt(-100, 101);

      // Return the array.     
      return array;     
   } 

   // Printing the given array.
   public static void printArray(int[] array)
   {
     String text = "\n";
     for (int number : array) {
        text += number + " >>> ";
     }
     text += "\n";
     System.out.println(text);
   }

   public static void main(String[] args) {

    // Generating an array of size 10.....
    System.out.println("\nGenerating an array of size 10.....");
    int integerArray[] = getRandomIntegerArray(10);
    
    // Printing the array.....
    System.out.print("\n[Integer Array]:");
    printArray(integerArray);
    
   } 
}

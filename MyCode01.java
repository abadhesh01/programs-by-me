 
 // Author: Abadhesh Mishra [Employee Id: 8117322]

 // Problem: Check two different strings are "anagrams" or not.

public class MyCode01
 {

    public static boolean checkAnagram(String string1, String string2)
    {
       // Step 1: Check the length of the two strings. 
       int stringLength = string1.length();
       if(stringLength != string2.length())
        return false;

       // Step 2: If two strings are equal, then return 'true'.
       if(string1.equals(string2)) 
        return true;

       // Step 3: Create an array to keep track of number  
       // of occourances of each character. 
       // For more refer: https://www.javatpoint.com/java-ascii-table
       int characterCount[] = new int[128];

       // Step 4: Iterate through the indices of both the strings.
       for(int index = 0; index < stringLength; index ++)
       {
        // Step 4.1: Increase by one for each occourance in the 1st string.
        char charAtFirstStringIndex = string1.substring(index, index + 1).charAt(0);
        characterCount[charAtFirstStringIndex] ++;

        // Step 4.2: Decrease by one for each occourance in the 2nd string.
        char charAtSecondStringIndex = string2.substring(index, index + 1).charAt(0);
        characterCount[charAtSecondStringIndex] --;
       }

       // Step 5: Check count for each character's occourance.
       for(int index = 0; index < characterCount.length; index ++) {
        // Step 5.1: If the number of occourance of any character is not 0(ZERO),
        // then return 'false'.
        if(characterCount[index] != 0)
        return false;
       }
        
       // Step 6: return 'true' as all the test cases passed.
       return true;
    }

    public static void main(String[] args) {
        
        // Cteate scanner object for keyboard input.
        java.util.Scanner scanner = new java.util.Scanner(System.in);

        // Input : 1st String
        System.out.print("\nEnter 1st string: ");
        String string1 = scanner.nextLine();

        // Input : 2nd String
        System.out.print("Enter 2nd string: ");
        String string2 = scanner.nextLine();

        // Output
        System.out.println("\n\"" + string1 + "\" and \"" + string2 
        + "\" are anagrams: " + checkAnagram(string1, string2) + "\n");

        // Close scanner object / resources.
        scanner.close();
    }
 }
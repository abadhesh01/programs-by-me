
// Author: Abadhesh Mishra [Employee Id: 8117322]
 
// Problem: Find the factorial of a given number using recursion.

public class MyCode15 {

    // The below method calculates the factorial of a given number recursively.
    public static int factorialOf(int number)
    {
        if(number == 0)
           return 1;
        else if(number == 1)
           return 1;
        else 
           return number * factorialOf(number - 1);     
    }

    public static void main(String[] args) {
       
        // Finding the factorial of numbers from 0 to 9.....
        System.out.println("\nFinding the factorial of numbers from 0 to 9.....");

        System.out.printf("\n%-20s %s\n", "Number", "Factorial");
        System.out.printf("%-20s %s\n", "------", "---------");
        for(int number = 0; number <= 9; number ++)
        {
            System.out.printf("%-20s %s\n", number, factorialOf(number));
        }

        System.out.println();
    }
}

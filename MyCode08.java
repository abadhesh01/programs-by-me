
// Author: Abadhesh Mishra [Employee Id: 8117322]

/*
 * Problem: Fanny's Occourances:
 * -----------------------------
 * Input:
 * 1. A string.
 * 2. A character.
 * 
 * Output:
 * Remove the given character from the given string.
 */

public class MyCode08 {

   // The below method removes all the occourances of the given character
   // from the given string. 
   public static String removeAllCharacters(String string, char character)
   {
    
     // Getting the length of the given string.     
     int stringLength = string.length();

     // Iterating through all the indices of the given string.
     for(int index = 0; index < stringLength; index ++)
     {
        // Condition for removing the character from the given         
        // string if the character is not at index 0.
        if ((index != 0) &&
            (string.charAt(index) == character)) 
        {
           // Removing the character from the string.
           string = string.substring(0, index) 
                  + string.substring(index + 1, stringLength);

           // Decreasing the index by 1 to prevent skipping of any characater
           // as after removal of the character, the current index is occupied
           // by the next character and it will be checked in the next iteration.
           // So, we need to prevent the updation of index to keep it same for the 
           // next iteration.        
           index --;
           
           // Decreasing the string length as it is one character less now.
           stringLength --; 

           // As we are already at a non zero index, no need to 
           // continue further. So skipping the current iteration.
           continue;
        } 

        // Condition for removing the character from          
        // the given string if the character is at index 0.
        if ((index == 0) && 
            (string.charAt(index) == character))
        {
            // Removing the character from the string.
            string = string.substring(1, stringLength);

            // Decreasing the index by 1 to prevent skipping of any characater
            // as after removal of the character, the current index is occupied
            // by the next character and it will be checked in the next iteration.
            // So, we need to prevent the updation of index to keep it same for the 
            // next iteration. 
            index = -1;

            // Decreasing the string length as it is one character less now.
            stringLength --;
        }
     }

     // Returning the string with given character removed.
     return string;
   }

  public static void main(String[] args) {

    System.out.println("\nRemoving a character throughout the given strings.....");

    // Declaring the number of test cases.
    int numberOfTestCases = 7;

    // Creating two arrays of test cases and characters to be removed.
    String []testCases = new String[numberOfTestCases];
    char []charactersToBeRemoved = new char[numberOfTestCases];
    
    // Test Case 1:
    testCases[0] = "XXXWelcoXXme tXo XXXXXmyX hXomeXXX.XXXXXXX";
    charactersToBeRemoved[0] = 'X';

    // Test Case 2:
    testCases[1] = "ZZZZZZZ";
    charactersToBeRemoved[1] = 'Z';

    // Test Case 3:
    testCases[2] = "@We@@lco@@me @@@t@o@ G@@ur@u@gra@@m@.@";
    charactersToBeRemoved[2] = '@';

    // Test Case 4:
    testCases[3] = "#C#a#m#e#r#o#o#n# #D#o#n#a#l#d#";
    charactersToBeRemoved[3] = '#';

    // Test Case 5:
    testCases[4] = "SISU";
    charactersToBeRemoved[4] = 'S';

    // Test Case 6:
    testCases[5] = " Do not remove anything except blank spaces. ";
    charactersToBeRemoved[5] = ' ';

    // Test Case 7:
    testCases[6] = "Remove nothing";
    charactersToBeRemoved[6] = '$';

    // Output:
    // Iterating through the test cases, generating and printing the outputs.
    for(int index = 0; index < numberOfTestCases; index ++) 
    {
       System.out.println("\nText:\n\"" + testCases[index] + "\"");
       System.out.println("Text after removing '"
                          + charactersToBeRemoved[index] + "':\n\""
                          + removeAllCharacters(testCases[index], charactersToBeRemoved[index]) 
                          + "\"");
    }
    
    System.out.println();
  }    
}


// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Print all the possible permutations of the given string.

// Reference: https://www.youtube.com/watch?v=vKQ6oUH02gw

public class MyCode14
{
   // The below method prints all the possible permutations of the given character array.
   public static void printPermutation(char[] characterArray, int fixedIndex)
   {
      // Termination criteria: When the 'fixexdIndex' reaches the last index
      // of the given character array, print the result and return.
      if(fixedIndex == (characterArray.length -1)) 
      {
        System.out.println(characterArray);
        return;   
      }

      // Treaverse through all the indices of the given character array.  
      for(int index = fixedIndex; index < characterArray.length; index ++)
      {
        // Swap the current index value with the fixed index value.
        swap(characterArray, index, fixedIndex);
        // Recursively call the current function with the next fixed index.
        printPermutation(characterArray, fixedIndex + 1);
        // Swap the current index value with the fixed index value
        // to restore the order of the characters.
        swap(characterArray, index, fixedIndex);
      }
   }

   // The below method swaps two indices of the given characater array,
   // provided the two indices. 
   public static void swap(char[] characterArray, int index, int index2)
   {
      char onHold = characterArray[index];
      characterArray[index] = characterArray[index2];
      characterArray[index2] = onHold;
   }

  public static void main(String[] args) {
     
      System.out.println("\nPrinting the permutation of a given string.....");
      // Creating a scanner class object to take keyboard input. 
      java.util.Scanner scanner = new java.util.Scanner(System.in);
      
      System.out.print("\nEnter your text: ");
      // Input:
      String text = scanner.nextLine();
      // Closing the scanner object after use to prevent the resource leak.
      scanner.close();

      System.out.println("\nPermutations of \"" + text + "\":");
      // Output:
      printPermutation(text.toCharArray(), 0);  

      System.out.println();
  }  
}


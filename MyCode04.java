
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Reverse a given string.

public class MyCode04
{
    public static void main(String[] args) {
        
    // Generating a random string..... 
    StringBuilder text =  new StringBuilder(MyCode03.getRandomString());
    System.out.println("\nText: " + text);
       
    // Revrsing the string.....
    System.out.println("\nReversing the text....."); 
    
    /*
       Steps of reversing a string:
       ----------------------------
       [Step 1]: Get the indices of starting and ending index.
       [Step 2]: Iterate untill either the starting index crosses
                 the ending index or the starting index and ending 
                 index meet each other.
       [Step 3]: Swap the elements of both the indices.
       [Step 4]: Increase the starting index by 1 and
                 decrease the ending index by 1.
       [Step 5]: Go to [Step 2].
    */

    // [Step 1]
    int startingIndex = 0; 
    int endingIndex = text.length() - 1;
      
    // [Step 2] & [Step 5]
    while (startingIndex < endingIndex) {
        
       char characterAtStartingIndex = text.charAt(startingIndex);
       char characterAtEndingIndex = text.charAt(endingIndex);
    
       // [Step 3]
       text.deleteCharAt(startingIndex);
       text.insert(startingIndex, characterAtEndingIndex);

       text.deleteCharAt(endingIndex);
       text.insert(endingIndex, characterAtStartingIndex);
       
       // [Step 4]
       startingIndex ++;
       endingIndex --;
    }
       
    System.out.println("\nText: " + text + "\n");
  }
}

// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Get the minimum element and maximum element of an array and find their sum.

public class MyCode06 {
  public static void main(String[] args) {
    
    // Generating an random array of size 10.....
    System.out.println("\nGenerating an random array of size 10.....");
    int integerArray[] = MyCode05.getRandomIntegerArray(10);

    // Printing the array.....
    MyCode05.printArray(integerArray);

    // Finding max and min value of the above array.....
    int max = integerArray[0];
    int min = integerArray[0];
    
    // Iterating through the indices of the given array.
    for(int index = 1; index < integerArray.length; index ++)
    {
        // If the current element is smaller than the minimum element,  
        // then updating the minimum element with current element.
        if(integerArray[index] < min)
           min = integerArray[index];   

        // If the current element is larger than the maximum element,  
        // then updating the maximum element with current element.   
        if(integerArray[index] > max)
           max = integerArray[index];
    }

    // Printing the element with maximum value, 
    // element with minimum value and their sum.
    System.out.println("Min element = " + min);
    System.out.println("Max element = " + max);
    System.out.println("Sum of min and max element: " + (max + min) + "\n");

  }    
}

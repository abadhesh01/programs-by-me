
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Remove duplicates from the given array.

public class MyCode12 
{

  // The below method removes all the duplicates from the given array.  
  public static int[] removeDuplicates(int[] array)
  {
    // If the array is empty or has only one element then returning the array.
    if(array.length == 0 || array.length == 1)
       return array;
   
    // Getting the max value of the given array. 
    int maxValue = array[0];
    for(int index = 1; index < array.length; index ++)
        if(array[index] > maxValue)
           maxValue = array[index];

    // Keeping track of number of occourances of each element of
    // the given array in anathor array of size about [(max value of given array) + 1].
    int[] trackCount = new int[maxValue + 1]; // This array keeps track of number  
                                              // of occourances of each elemet.
    for(int index = 0; index < array.length; index ++)
        trackCount[array[index]] ++;

    // Getting the actual number of elements on removal of duplicate elements
    // and creating anathor array of the size of actual number of elements to  
    // store the result.    
    int actualNumberOfElements = 0; // This variable keeps track of actual number of elements.
    for(int index = 0; index < trackCount.length; index ++)
        if(trackCount[index] > 0)
           actualNumberOfElements ++;
    int[] result = new int[actualNumberOfElements];  // Array of actual size to store result.
    
    // Storing the result in the final array.
    int oldArrayIndex = 0, newArrayIndex = 0; // Initializing the given array 
                                              // and result array indices.
    while(oldArrayIndex < array.length /*Traversing through given array indices.*/)
    {
        // If the track count of any index is greater than 0(ZERO),
        // then storing that index in the result array, incrementing 
        // the result array index by 1 and setting the track count to 0(ZERO).
        if(trackCount[array[oldArrayIndex]] > 0)
        {
          result[newArrayIndex ++] = array[oldArrayIndex];
          trackCount[array[oldArrayIndex]] = 0;
        }   
        oldArrayIndex ++; // Incrementing the given array index by 1.  
    }

    // Returning the result.
    return result;
  }  

  public static void main(String[] args) {
    
    // Testing the method removeDuplicates() and printing the output.
    System.out.println("\nRemoving duplicates from the given set of arrays..........\n");

    int[][] array = {
        {0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4},
        {0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2},
        {1, 5, 3, 5, 5, 9, 7, 7, 9, 4, 5, 6, 8, 4, 7},
        {1, 3, 7, 3, 5, 7, 0, 4, 9, 8, 1, 5, 7, 8, 6},
        {9, 6, 6, 8, 1, 9, 4, 4, 6, 4, 8, 2, 5, 7, 0},
        {1, 1},
        {3, 4},
        {1},
        {}
    };

     for(int row = 0; row < array.length; row ++) 
     {
        System.out.println("-----------------------------------" + 
        "------------------------------------------------------");
        int currentArray[] = array[row];
        System.out.print("Given array: ");
        MyCode05.printArray(currentArray);
        System.out.print("Given array after removal of duplicates: ");
        currentArray = removeDuplicates(currentArray); 
        MyCode05.printArray(currentArray);
     }
  }    
}

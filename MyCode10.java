
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Swap two numbers without any third variable.

public class MyCode10 {
  public static void main(String[] args) {

      int x = 17;
      int y = 30; 
      // x = 17 & y = 30

      System.out.println("\nx = " + x + ", y = " + y);

      // Swapping
      System.out.println("\nAfter swapping.............");
      x = x + y;
      // x = 47 & y = 30
      y = x - y;
      // x = 47 & y = 17
      x = x - y;
      // x = 30 & y = 17

      System.out.println("\nx = " + x + ", y = " + y);
      System.out.println();
  }    
}

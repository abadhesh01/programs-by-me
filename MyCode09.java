
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Rotate an given array by given number of steps. 

public class MyCode09 {

  // The below method rotates an given array by given number of steps.
  public static int[] rotateArray(int []array, int numberOfSteps) {
 
       int numberOfSwaping = array.length; // Defining the number of swaping needed to be done.
       int index = 0; // Declaring and initializing the index.
       int holdElement = array[index]; // Holding the element of the initialized index (i.e. current index). 
       
       // Reducing the unnecessary number of steps to significant number(s) if required.
       numberOfSteps = numberOfSteps % array.length;

       // If the number of steps is zero, returning the array as no swapping is required.
       if (numberOfSteps == 0) {
           return array;
       }

       while(numberOfSwaping > 0 /*Checking the number of swapping(s) remaining.*/)  
       { 
        // If we are rotating the given array clockwise and the number of steps
        // after the current index is exceeding the index range, then counting   
        // the additional steps from index 0(ZERO).
        if((index + numberOfSteps) >= array.length) 
           index = (index + numberOfSteps) - array.length;
        // If we are rotating the given array anti-clockwise and the number of steps
        // after the current index is exceeding the index range, then counting   
        // the additional steps from last index of the array i.e. [array.length - 1].
        else if((index + numberOfSteps) < 0)
           index = (index + numberOfSteps) + array.length;  
        // If the number of steps after the current index is not exceeding the index range,  
        // then the step count is normal i.e. [(current index) + (number of steps)].     
        else
           index = index + numberOfSteps;    

        // Swapping the holding element value with current index value after step count.   
        int temp = array[index];
        array[index] = holdElement;
        holdElement = temp;

        // Decreasing the number of swapping(s) by 1 after afetr each swapping.
        numberOfSwaping --;  
       }

    // Returning the array.   
    return array;
  }  

  public static void main(String[] args) {

    System.out.println("\nRotating given array by given number  of steps.....\n");

    // Creating a dummy array.
    int sampleArray[] = {1, 2, 3, 4, 5, 6, 7}; 
    // Craeting a copy of above array to prevent updation. 
    int copyOfSampleArray[] = new int[sampleArray.length]; 

    // Testing the rotateArray() method with the dummy array and with the 
    // different number of steps from -13 to 13 and displaying the output. 
    for (int numberOfSteps = -13; numberOfSteps <= 13; numberOfSteps ++)
    {  
      System.out.println("*****************************************");  
      System.out.println("Number of rotations: " + numberOfSteps);  

      System.arraycopy(sampleArray, 0, copyOfSampleArray, 0, sampleArray.length);
      System.out.print("\nArray before rotation: ");
      MyCode05.printArray(copyOfSampleArray);

      copyOfSampleArray = rotateArray(copyOfSampleArray, numberOfSteps);
      System.out.print("Array after rotation: ");
      MyCode05.printArray(copyOfSampleArray);

    }
  }     
}

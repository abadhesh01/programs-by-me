
// Author: Abadhesh Mishra [Employee Id: 8117322]

// Problem: Check two different strings are "anagrams" or not.

import java.util.Arrays;

public class MyCode02 {

    public static boolean checkAnagram(String string1, String string2) {

    // Step 1: Check the length of the two strings. 
    int stringLength = string1.length();
    if (stringLength != string2.length())
       return false;

    // Step 2: If two strings are equal, then return 'true'.
    if(string1.equals(string2)) 
       return true;

    // Step 3: Convert both the strings to array of characters.
    char stringCharacterArray1[] = string1.toCharArray();
    char stringCharacterArray2[] = string2.toCharArray();

    // Step 4: Sort the array of characters.
    Arrays.sort(stringCharacterArray1);
    Arrays.sort(stringCharacterArray2);
 
    // Step 5: Compare each index of both the character arrays and 
    // if values of any pair of indices are not equal, return 'false'. 
    for(int index = stringLength - 1; index >= 0; index --)
        if(stringCharacterArray1[index] != stringCharacterArray2[index])
           return false;
      
    // Step 6: return 'true' as all the test cases are passed.
        return true;
    }

    public static void main(String[] args) {
        
        // Cteate scanner object for keyboard input.
        java.util.Scanner scanner = new java.util.Scanner(System.in);

        // Input : 1st String
        System.out.print("\nEnter 1st string: ");
        String string1 = scanner.nextLine();

        // Input : 2nd String
        System.out.print("Enter 2nd string: ");
        String string2 = scanner.nextLine();

        // Output
        System.out.println("\n\"" + string1 + "\" and \"" + string2 
        + "\" are anagrams: " + checkAnagram(string1, string2) + "\n");

        // Close scanner object / resources.
        scanner.close();
    }
}
